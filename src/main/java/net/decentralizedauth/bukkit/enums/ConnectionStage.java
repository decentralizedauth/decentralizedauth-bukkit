package net.decentralizedauth.bukkit.enums;

public enum ConnectionStage {
    AWAITING_START,
    KEY_EXCHANGE,
    AUTHENTICATING,
    PROFILE_EXCHANGE,
    SUCCESSFUL,
    FAILED
}

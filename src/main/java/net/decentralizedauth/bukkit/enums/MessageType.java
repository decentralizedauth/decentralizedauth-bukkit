package net.decentralizedauth.bukkit.enums;

public enum MessageType {
    INFO,
    WARNING,
    ERROR
}

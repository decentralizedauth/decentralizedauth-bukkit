package net.decentralizedauth.bukkit;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import net.decentralizedauth.bukkit.enums.MessageType;
import net.decentralizedauth.bukkit.listeners.HandshakeListener;
import net.decentralizedauth.bukkit.listeners.PingPacketListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public final class DecentralizedAuth extends JavaPlugin {

    private static final Logger logger = Bukkit.getLogger();
    private static final String prefix = "[Decentralized Auth]: ";

    @Override
    public void onEnable() {
        //Manages all the packets/exposes the lib api. Inner working is black magic, DO NOT TOUCH
        ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();
        protocolManager.addPacketListener(new HandshakeListener(this, ListenerPriority.HIGHEST, PacketType.Handshake.Client.SET_PROTOCOL));
        protocolManager.addPacketListener(new PingPacketListener(this, ListenerPriority.HIGHEST, PacketType.Status.Server.SERVER_INFO));
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public static void log(String message, MessageType type){
        switch (type){
            case INFO:
                //use for anything that doesnt fit in the two below
                logger.info(prefix+message);
                break;
            case WARNING:
                //use for something that, while expected, isint recommended and could turn into an issue
                logger.warning(prefix+message);
                break;
            case ERROR:
                //use for something that really should NEVER be happening
                System.err.println(prefix+message);
        }
    }
}

package net.decentralizedauth.bukkit.handlers;

import com.comphenix.protocol.ProtocolManager;
import net.decentralizedauth.bukkit.enums.ConnectionStage;
import net.decentralizedauth.bukkit.listeners.HandshakeListener;
import org.bukkit.entity.Player;

public class CustomAuthHandler {

    private Player player;
    private ProtocolManager protocolManager;
    private HandshakeListener handshakeListener;

    private ConnectionStage connectionStage;

    public CustomAuthHandler(Player player, ProtocolManager protocolManager, HandshakeListener handshakeListener){
        this.player = player;
        this.protocolManager = protocolManager;
        this.handshakeListener = handshakeListener;
        this.connectionStage = ConnectionStage.AWAITING_START;
    }

}

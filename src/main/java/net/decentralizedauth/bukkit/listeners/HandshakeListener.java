package net.decentralizedauth.bukkit.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerOptions;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import net.decentralizedauth.bukkit.DecentralizedAuth;
import net.decentralizedauth.bukkit.enums.MessageType;
import net.decentralizedauth.bukkit.handlers.CustomAuthHandler;
import org.bukkit.plugin.Plugin;

import javax.annotation.Nonnull;
import java.util.ArrayList;

public class HandshakeListener extends PacketAdapter {

    private final ArrayList<CustomAuthHandler> authHandlers = new ArrayList<>();

    public HandshakeListener(@Nonnull AdapterParameteters params) {
        super(params);
    }

    public HandshakeListener(Plugin plugin, PacketType... types) {
        super(plugin, types);
    }

    public HandshakeListener(Plugin plugin, Iterable<? extends PacketType> types) {
        super(plugin, types);
    }

    public HandshakeListener(Plugin plugin, ListenerPriority listenerPriority, Iterable<? extends PacketType> types) {
        super(plugin, listenerPriority, types);
    }

    public HandshakeListener(Plugin plugin, ListenerPriority listenerPriority, Iterable<? extends PacketType> types, ListenerOptions... options) {
        super(plugin, listenerPriority, types, options);
    }

    public HandshakeListener(Plugin plugin, ListenerPriority listenerPriority, PacketType... types) {
        super(plugin, listenerPriority, types);
    }

    @Override
    public void onPacketReceiving(PacketEvent event) {
        DecentralizedAuth.log("Handshake detected!", MessageType.INFO);
        if (event.getPacketType().getClass() != PacketType.Handshake.Client.SET_PROTOCOL.getClass()) {
            throw new IllegalStateException("Received invalid packet type!");
        }

        //per the minecraft protocol, the nextID is the second int registered in the packet
        int nextId = event.getPacket().getIntegers().read(1);

        /* now, find the proper int field by going through all fields in the enum
        we know from looking at the code that the field we want is the third integer to be loaded
        therefore, cycle through until we count 3 ints. */
        int intCount = 0;
        if (nextId == 69) {
            DecentralizedAuth.log("Handshake is one of ours! Initiating custom auth!", MessageType.INFO);
            //authHandlers.add(new CustomAuthHandler(event.getPlayer(), protocolManager, handshakeListener));
            //not doing that for debug purposes at the moment
        } else {
            DecentralizedAuth.log("Handshake is vanilla, aborting auth", MessageType.INFO);
        }
    }
}

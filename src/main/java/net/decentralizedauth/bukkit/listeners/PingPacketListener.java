package net.decentralizedauth.bukkit.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerOptions;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.FieldAccessException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.decentralizedauth.bukkit.DecentralizedAuth;
import net.decentralizedauth.bukkit.enums.MessageType;
import org.bukkit.plugin.Plugin;

import javax.annotation.Nonnull;
import java.util.function.Function;

public class PingPacketListener extends PacketAdapter {

    private static final Gson gson = new Gson();
    private static final Function<String, String> metadataTransformer = str -> {
        JsonObject json = gson.fromJson(str, JsonObject.class);
        json.addProperty("decentralizedAuth", 1);
        return json.toString();
    };

    public PingPacketListener(@Nonnull AdapterParameteters params) {
        super(params);
    }

    public PingPacketListener(Plugin plugin, PacketType... types) {
        super(plugin, types);
    }

    public PingPacketListener(Plugin plugin, Iterable<? extends PacketType> types) {
        super(plugin, types);
    }

    public PingPacketListener(Plugin plugin, ListenerPriority listenerPriority, Iterable<? extends PacketType> types) {
        super(plugin, listenerPriority, types);
    }

    public PingPacketListener(Plugin plugin, ListenerPriority listenerPriority, Iterable<? extends PacketType> types, ListenerOptions... options) {
        super(plugin, listenerPriority, types, options);
    }

    public PingPacketListener(Plugin plugin, ListenerPriority listenerPriority, PacketType... types) {
        super(plugin, listenerPriority, types);
    }

    @Override
    public void onPacketSending(PacketEvent event) {
        DecentralizedAuth.log("Server info ping detected!", MessageType.INFO);
        if (event.getPacketType().getClass() != PacketType.Status.Server.SERVER_INFO.getClass()) {
            throw new IllegalStateException("Sending invalid packet type!");
        }
        try {
            String json = event.getPacket().getStrings().read(0);
            event.getPacket().getModifier().write(0, metadataTransformer.apply(json));
        } catch (FieldAccessException | JsonSyntaxException e) {
            DecentralizedAuth.log(e.getMessage(), MessageType.ERROR);
        }
    }
}
